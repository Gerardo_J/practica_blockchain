const SHA256 = require('crypto-js/sha256');

class Block {
    // Index = Posición del blocque en la cadena.
    // Data = El contenido del bloque en la caden.
    // PreviousHash = Valor del bloque anterior al cifrado.
    // Date = fecha de la creación del bloque.
    // hash = Validación como cadena valida.
    // noune = Número aleatorio de la cadena. 

    constructor(index, data, previousHash=''){
        this.index = index;
        this.data = data;
        this.previousHash = previousHash;
        this.nounce = 0;
        this.hash = this.createHash();
    }

    createHash(){
        const originalChain = `${this.index}|${this.data}|${this.date}|${this.nounce}`
        return SHA256(originalChain).toString();
    }

    mine(dif){
        while(!this.hash.startsWith(dif)){
            this.nounce++;
            this.hash = this.createHash();
        }
    }
}

module.exports = Block;

